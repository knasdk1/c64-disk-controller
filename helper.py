import curses
import os


def selection_list(stdscr, items, select_message, cancel=True):
    if 'Cancel' not in items and cancel is True:
        items.append('Cancel (press escape)')

    attributes = {}
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)
    attributes['normal'] = curses.color_pair(1)

    curses.init_pair(2, curses.COLOR_CYAN, curses.COLOR_BLUE)
    attributes['highlighted'] = curses.color_pair(2)

    curses.init_pair(3, curses.COLOR_CYAN, curses.COLOR_BLUE)
    stdscr.bkgd(' ', curses.color_pair(3) | curses.A_BOLD)

    key_code = 0  # last character read
    option = 0  # the current option that is marked
    
    while key_code != 10:  # Enter in ascii
        stdscr.erase()
        stdscr.addstr(select_message + "\n\n", curses.A_UNDERLINE)
        for i in range(len(items)):
            if i == option:
                attr = attributes['highlighted']
            else:
                attr = attributes['normal']
            stdscr.addstr("{0}. ".format(i + 1))
            stdscr.addstr(items[i] + '\n', attr)
        key_code = stdscr.getch()
        if key_code == curses.KEY_UP and option > 0:
            option -= 1
        elif key_code == curses.KEY_DOWN and option < len(items) - 1:
            option += 1
        elif key_code == 27:
            return None

    #stdscr.addstr("You chose {0}".format(items[option]))
    #stdscr.getch()
    if items[option] == 'Cancel':
        return None
    
    return items[option]


def clear_screen(message=''):
    os.system('clear')
    if message != '':
        print(message)


def wait_for_press_enter(message=None):
    out_message = "\nPress enter"
    if message is not None:
        out_message = "\n" + message + out_message
    input(out_message)
    clear_screen()


def keyboard_interrupt_handler(signal, frame):
    os.system('echo -ne "\\033]11;#000000\\07"')
    os.system('clear')
    exit(0)
