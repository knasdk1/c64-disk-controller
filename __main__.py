#!/usr/bin/python
# python3 -m zipapp C642 -o=C6422 -p=/usr/bin/python3
import curses
import os
import signal
from DiskController import DiskController
from helper import selection_list, clear_screen, wait_for_press_enter, keyboard_interrupt_handler

signal.signal(signal.SIGINT, keyboard_interrupt_handler)

disk = DiskController()
messages = {
    'loading': 'Please wait. Loading disk...',
    'wait': 'Please wait...',
}
menu = [
    'LOAD"$",8,1',

    'Make disk image from 1541',
    'Copy file from 1541',

    'Restore disk image to 1541',
    'Format disk',
    'Reset disk',

    'Exit',
]

os.system('echo -ne "\\033]11;#3465a4\\07"')
os.system('echo -ne "\033[1m"')
os.system('echo -ne "\033[36mCyan"')

clear_screen()

while True:
    selection = curses.wrapper(selection_list, menu, "Select option:", False)

    if selection == 'Copy file from 1541':
        clear_screen(messages['loading'])
        if disk.copy_file_from_1541() is True:
            wait_for_press_enter()

    elif selection == 'Format disk':
        clear_screen()
        print('Format disk - are you sure (y/n)? ', end='')
        confirm = input()
        if confirm == 'y':
            disk.format()
        clear_screen()

    elif selection == 'LOAD"$",8,1':
        clear_screen(messages['loading'])
        files = disk.get_dir(False)
        clear_screen()
        print(files)
        wait_for_press_enter()

    elif selection == 'Reset disk':
        clear_screen(messages['wait'])
        disk.reset()

    elif selection == 'Make disk image from 1541':
        clear_screen('Make disk image from 1541')
        disk.backup_image()
        wait_for_press_enter()

    elif selection == 'Restore disk image to 1541':
        clear_screen('Restore disk image to 1541')
        error = disk.restore_image()
        if error is False:
            wait_for_press_enter(error)
        else:
            wait_for_press_enter()

    elif selection == 'Exit':
        os.system('echo -ne "\\033]11;#000000\\07"')
        os.system('clear')
        exit(0)
