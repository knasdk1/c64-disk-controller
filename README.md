# Controller for [ZoomFloppy](http://store.go4retro.com/zoomfloppy/)
This is a controller for basic interactions with the Commodore 1541 disk drive, written in Python 3
## Usage
The file *c64* is an executable packed version of the Python program. Just run it from the folder `./c64`  
To run the program via the source code, standing in the folder of the files type: `python3 .` or outside the folder `python3 [path_to_folder]`  
  
It interacts with the [ZoomFloppy](http://store.go4retro.com/zoomfloppy/) interface, using functionality from the open source [OpenCBM](https://opencbm.trikaliotis.net/opencbm-8.html).
# Setup [OpenCBM](https://opencbm.trikaliotis.net/opencbm-8.html)
First you need to get the sourcde code for OpenCBM.
- Get the sources from the tarball at http://www.trikaliotis.net/Download/opencbm-0.4.99.99/opencbm-0.4.99.99-source.tar.bz2.
- Unpack them into a directory (i.e., ~/opencbm-0.4.99.99, with 0.4.99.99 being the version number of OpenCBM).
- cd into the directory: cd ~/opencbm-0.4.99.99
## Compile the source code
- sudo apt-get install libncurses5-dev
- sudo apt-get install libusb-dev
- make -f LINUX/Makefile opencbm plugin-xum1541
- sudo make -f LINUX/Makefile install install-plugin-xum1541
- If you get an error message that libopencbm cannot be found, you must add it to your linker path. In order to do so, enter the command make -f LINUX/Makefile ldconfig
- sudo usermod -a -G users *[your linux username]*
