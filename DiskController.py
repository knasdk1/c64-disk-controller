from helper import selection_list
import os
import re
import curses


class DiskController:

    def __init__(self):
        pass

    def create_file_path(self, path, check_file_exist=False):
        file_data = path.split('/')

        file_name = file_data.pop()
        if file_name[-4:].lower() != '.d64':
            file_name += '.d64'

        file_path = '/'.join(file_data)
        if file_path[-1:] != '/':
            file_path += '/'

        full_path = os.path.expanduser(file_path + file_name)

        if check_file_exist and os.path.isfile(full_path) is not True:
            raise FileExistsError('File "' + full_path + '" does not exist')

        return full_path

    def get_dir(self, as_list=True):
        files = os.popen('cbmctrl dir 8').read()
        if as_list:
            return files.split("\n")

        return files

    def select_file_from_1541(self):
        file_data = curses.wrapper(selection_list, self.get_dir(), 'Select file to copy')
        if file_data is None:
            return False

        return file_data

    def copy_file_from_1541(self):
        source_file_data = self.select_file_from_1541()
        if not source_file_data:
            return False

        file_name = re.search("\"(.*)\"", source_file_data).group(1)
        file_type = re.search("\".*\"[ ]{1,}([\d\w]+)", source_file_data).group(1)

        path = input('Path to file on PC [default "~/" - "c" to abort]: ')
        if path == 'c':
            return False
        if path == '':
            path = '~/'
        if path[-1:] != '/':
            path += '/'

        os.system('cd ' + path + '; cbmcopy -r 8 "' + file_name + '" -o ' + re.escape(file_name) + '.' + file_type)

        return True

    def format(self):
        diskName = input('Enter disk name: ')
        os.system('cbmformat -p 8 ' + diskName + ',42')

    def reset(self):
        os.system('cbmctrl reset')

    def backup_image(self):
        file_data = input('Path and file name to write image to ["c" to abort]: ')
        if file_data == 'c':
            return False

        path_data = self.create_file_path(file_data)
        os.system('d64copy 8 ' + re.escape(path_data))

    def restore_image(self):
        file_data = input('Path and file name to image you want to restore ["c" to abort]: ')
        if file_data == 'c':
            return False

        try:
            path_data = self.create_file_path(file_data, True)
        except FileExistsError as e:
            return str(e)

        os.system('d64copy ' + re.escape(path_data) + ' 8')
